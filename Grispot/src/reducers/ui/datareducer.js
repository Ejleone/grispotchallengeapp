import {
  FETCH_PARTICIPANTS_ADD,
  PARTICIPANTS_TOGGLE,
} from '../../actions';

const initialState = {
  participants: ["tonyb", "kevint", "david"],
  loading: false,
  error: null
};

export const datareducer(state = initialState, action) {
  switch(action.type) {
    case FETCH_PARTICIPANTS_ADD:
      // Mark the state as "loading" so we can show a spinner or something
      return {
        ...state,
        loading: false,
        error: null
      };
      case PARTICIPANTS_TOGGLE:
        // Mark the state as "loading" so we can show a spinner or something
        return {
          ...state,
          loading: false,
          error: null
        };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}
export default datareducer;
