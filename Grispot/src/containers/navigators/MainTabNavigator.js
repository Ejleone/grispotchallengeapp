import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Platform } from 'react-native';
import {
    StackNavigator,
    TabNavigator,
} from 'react-navigation';

import * as Routes from '../../constants/routes';

import {
        participants,
        workouts,
        newuser,} from '../../screens/';

console.log(workouts);
console.log(participants);
console.log("name" + [Routes.NEW_ROUTE] +newuser);


const MainTabNavigator = StackNavigator({
  [Routes.WORK_ROUTE]: {screen: workouts},
  [Routes.PART_ROUTE]: {screen: participants},
  [Routes.NEW_ROUTE]: {screen:newuser},


},{ headerMode: `${Platform.OS === 'ios' ? 'float' : 'screen'}` });


export default MainTabNavigator;
