import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    StackNavigator,
} from 'react-navigation';

import * as Routes from '../../constants/routes';


import { MainTabNavigator } from './MainTabNavigator';
import { AppLoadingScreen } from '../app/ApploadingScreen';

export const RootNavigator = StackNavigator({
    [Routes.APP_LOADING_SCREEN]: { screen: AppLoadingScreen },
    [Routes.MAIN_ROUTE]: { screen: MainTabNavigator },
}, {
    headerMode: 'none',
});

export default RootNavigator;
