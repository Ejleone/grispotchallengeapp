/* @flow*/
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SectionList,
  TouchableOpacity,
} from 'react-native';

import { PART_ROUTE } from '../../constants/routes';

const wouts = [
  {
    id: 0,
    title: 'upper body',
    data: [
      {id: 0, text: 'shoulders'},
      {id: 1, text: 'chest'},
      {id: 2, text: 'hands'},
    ]
  },
  {
    id: 1,
    title: 'Torso',
    data: [
      {id: 3, text: 'Abs'},
      {id: 4, text: 'Back'},
    ]
  },
  {
    id: 1,
    title: 'Lower body',
    data: [
      {id: 3, text: 'legs'},
      {id: 4, text: 'Feet'},
    ]
  }
]
const extractKey = ({id}) => id

export class workouts extends Component {

  _click = () =>{

        this.props.navigation.navigate(PART_ROUTE)

  }

  renderWpart = ({item}) => {
    return (
     <TouchableOpacity onPress={ () => this._click()}>
      <Text style={styles.row}>
        {item.text}
      </Text>
     </TouchableOpacity >
    )
  }

  renderSectionHeader = ({section}) => {
    return (
      <Text style={styles.header}>
        {section.title}
      </Text>
    )
  }


  render() {
    return (
      <SectionList
           style={styles.container}
           sections={wouts}
           renderItem={this.renderWpart}
           renderSectionHeader={this.renderSectionHeader}
           keyExtractor={extractKey}
         />
    );
  }
}

const styles = StyleSheet.create({
  container: {
     marginTop: 20,
     flex: 1,
   },
   row: {
     padding: 15,
     marginBottom: 5,
     backgroundColor: 'skyblue',
   },
   header: {
     padding: 15,
     marginBottom: 5,
     backgroundColor: 'steelblue',
     color: 'white',
     fontWeight: 'bold',
   },
});
*/
export default workouts;
