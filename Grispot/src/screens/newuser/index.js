/* @flow */
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,

} from 'react-native';



const { width, height } = Dimensions.get('window');
export class newuser extends Component {

  _click = () =>{

      console.log('click one');

        }

constructor(props) {
  super(props);

  this.state = {
    name : '',
  };
}

  render() {
    return (
      <View style={stylest.container}>



      <View style={stylest.inputWrap}>
                <View style={stylest.iconWrap}>
                  <Image source={{uri: 'http://80.211.145.215/Images/login1lock.png'}} style={stylest.icon} resizeMode="contain" />
                </View>
                <TextInput
                  placeholder="Name of participant"
                  placeholderTextColor="#000"
                  style={stylest.input}
                   onChangeText={(lpass) => this.setState.name}
                />
      </View>



      <TouchableOpacity activeOpacity={.5} onPress={ this._click()} >
         <View style={stylest.button}>
           <Text style={stylest.buttonText}>Add participant</Text>
         </View>
      </TouchableOpacity>
      </View>
    );
  }
}

const stylest = StyleSheet.create({
  container: {
     backgroundColor: '#fff',
     flex: 1,
   },
   inputWrap: {
   flexDirection: "row",
   marginVertical: 10,
   height: 40,
   borderBottomWidth: 1,
   borderBottomColor: "#CCC"
 },
 iconWrap: {
  paddingHorizontal: 7,
  alignItems: "center",
  justifyContent: "center",
},
icon: {
  height: 20,
  width: 20,
},
input: {
  flex: 1,
  paddingHorizontal: 10,
},
button: {
  backgroundColor: "#12a7e2",
  paddingVertical: 20,
  alignItems: "center",
  justifyContent: "center",
  marginTop: 30,
},
buttonText: {
  color: "#FFF",
  fontSize: 18,
},
});


export default newuser;
