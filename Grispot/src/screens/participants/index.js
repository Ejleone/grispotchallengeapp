/* @flow */

import React, { Component } from 'react';
import {
  View,
  Button,
  Text,
  StyleSheet,
  SectionList,
  TouchableOpacity,
} from 'react-native';

import { NEW_ROUTE} from '../../constants/routes';

const participantslist = [  {
    id: 0,
    title: 'participants',
    data: [
      {id: 0, text: 'elijah'},
      ]
  },]

const extractKey = ({id}) => id

export class participants extends Component {


  _click = () =>{

        this.props.navigation.navigate(NEW_ROUTE)

  }

  renderpart = ({item}) => {
    return (
    <View style={styles.list}>
      <Text style={styles.row}>
        {item.text}
      </Text>
      <Text style={styles.row1}>
        online
      </Text>

      <Button style={styles.row1}
         title="check in"/>
      </View>
    )
  }

  renderSectionHeader = ({section}) => {
    return (
      <Text style={styles.header}>
        {section.title}
      </Text>
    )
  }

  render() {
    return (
    <View  style={styles.container}>

    <TouchableOpacity activeOpacity={.5} onPress={ this._click()} >
       <View style={styles.button}>
         <Text style={styles.buttonText}>Add participant</Text>
       </View>
    </TouchableOpacity>

      <SectionList
           style={styles.container}
           sections={participantslist}
           renderItem={this.renderpart}
           renderSectionHeader={this.renderSectionHeader}
           keyExtractor={extractKey}
         />
    );

    </View>
  }
}

const styles = StyleSheet.create({
  container: {
     marginTop: 20,
     flex: 1,
   },
   list: {
    flexDirection: 'row',
     backgroundColor: 'skyblue',
   },

   row: {
     padding: 15,
     marginBottom: 5,

   },
   row1: {
     padding: 15,
     marginBottom: 5,
     color:'green',
   },
   header: {
     padding: 15,
     marginBottom: 5,
     backgroundColor: 'steelblue',
     color: 'white',
     fontWeight: 'bold',
   },
   button: {
     backgroundColor: "#12a7e2",
     paddingVertical: 20,
     alignItems: "center",
     justifyContent: "center",
     marginTop: 30,
   },
   buttonText: {
     color: "#FFF",
     fontSize: 18,
   },
});

export default participants;
